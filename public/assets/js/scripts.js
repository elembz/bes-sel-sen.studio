const wow = new WOW({
  offset: 75,
  callback: function(el) {
    $(el).removeClass("wow has-wow");
  }
});
wow.init();

let sliders = [];

function initiateSliders() {

  $(document).find(".js-slider").each(function() {
    const slider = $(this);
    const moveToNextSlide = function() {

      if ($("body").hasClass("portfolio")) {
        const activeSlide = slider.find(".slider__active-slide");
        let nextSlide = activeSlide.nextAll("img:first");
        if (nextSlide.length < 1) {
          nextSlide = slider.children(":first");
        }
        nextSlide.addClass("slider__active-slide");
        activeSlide.removeClass("slider__active-slide");
      }

    }
    setInterval( moveToNextSlide, 5000);

  })

}

function rotateLogo() {
  const direction = ($(window).scrollTop() > 50 ? "vertical" : "horizontal");
  const logo = $(".js-logo");
  if (direction === "vertical") logo.addClass("logo--vertical");
  else logo.removeClass("logo--vertical");
}

function goToSection(name) {

  const openedSection = $(".section--open");
  const openedSectionName = openedSection.data("name");
  const sectionToOpen = $(".section--" + name);
  const scrollPosition = $(window).scrollTop();
  const animationSpeed = scrollPosition > 0 ? 300 : 0;

  $('.js-page-header, .js-page-footer').removeClass('wow');

  $("html, body").animate({ scrollTop: 0 }, animationSpeed, function() {
    openedSection.addClass("section--closed").removeClass("section--open section--out-of-focus");
    sectionToOpen.removeClass("section--closed section--out-of-focus").addClass("section--open");
    $("body").removeClass(openedSectionName).addClass(name);

    $("video").each(function() {
      if (name === "portfolio") this.play();
      else this.pause();
    });

    openedSection.find(".has-wow").each(function() {
      $(this).removeClass("wow");
    });
    sectionToOpen.find(".has-wow").each(function() {
      $(this).addClass("wow");
    });

    wow.init();

  })

}

function expandFooter() {
  $(".js-page-footer").addClass("page-footer--expanded");
  $(".js-page-container").addClass("page-container--darkened");
}

function minimizeFooter() {
  $(".js-page-footer").removeClass("page-footer--expanded");
  $(".js-page-container").removeClass("page-container--darkened");
}

$(function() {

  rotateLogo();

  if (window.scrollY > $(window).height() ) {
    $(".wow").removeClass("wow has-wow");
  }

})

$(window).on("load", function() {

  initiateSliders();

  $(".section--closed").find(".has-wow").each(function() {
    if ($(this).offset().top > $(window).height()) {
      console.log(this);
      $(this).removeClass("wow");
    }
  });

  $(".page-container").removeClass("loading");

  $(".js-section").on("click", function(e) {
    if ($(this).hasClass("section--closed")) {
      goToSection($(this).data("name"));
    }
  });

  $(".js-page-container").on("click", function() {
    if ($(this).hasClass("page-container--darkened") && $(".js-footer").hasClass("page-footer--expanded")) {
      minimizeFooter();
    }
  })

  $(".to-other-page").on("click", function(e) {
    const sectionToOpen = $("body").hasClass("info") ? "portfolio" : "info";
    goToSection(sectionToOpen);
    e.preventDefault();
  })

  $(".js-page-footer").on("click", function (e) {
    if (!$(this).hasClass("page-footer--expanded")) {
      expandFooter();
      e.preventDefault();
    }
  });

  $(".js-page-footer").on({
    mouseover: function () {
      expandFooter();
    },
    mouseleave: function () {
      minimizeFooter();
    }
  });

  $(document).find(".js-section").on({
    mouseover: function () {
      if ($(this).hasClass("section--closed")) {
        $(".js-section.section--open").addClass("section--out-of-focus");
      }
      else {
        $(".js-section.section--open").removeClass("section--out-of-focus");
      }
    },
    mouseleave: function () {
      if ($(this).hasClass("section--closed")) {
        $(".js-section.section--open").removeClass("section--out-of-focus");
      }
    }
  });

});

$(window).scroll(function() {
  rotateLogo();
});
